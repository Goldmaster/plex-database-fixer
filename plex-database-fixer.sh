#!/bin/bash

# Change to the appdata directory
cd /mnt/user/appdata

# Check if either directory exists
if [ -d "binhex-plex" ] && [ -d "binhex-plexpass" ]; then
  # Prompt the user to select which directory to change to
  echo "Both binhex-plex and binhex-plexpass exist. Please select one to change to:"
  select dir in "binhex-plex" "binhex-plexpass"; do
    # Change to the selected directory and break out of the loop
    cd "$dir" && break
  done
elif [ -d "binhex-plex" ]; then
  # Change to the binhex-plex directory
  cd binhex-plex
elif [ -d "binhex-plexpass" ]; then
  # Change to the binhex-plexpass directory
  cd binhex-plexpass
else
  # Neither directory exists, so exit the script
  echo "Neither binhex-plex nor binhex-plexpass exists. Exiting."
  exit 1
fi

# Detect the container name from the folder name
containername=$(echo "${dir}" | sed 's/-//g')

# Check if the databasetools directory exists
if [ ! -d "databasetools" ]; then
  # Create the databasetools directory if it doesn't exist
  mkdir databasetools
fi

# Copy the files from the container to the destination folder
cp "$containername":/usr/lib/plexmediaserver/* /mnt/user/appdata/"$dir"/databasetools/

# Change to the plexmediaserver directory within databasetools
if [ -d "databasetools/plexmediaserver" ]; then
  cd databasetools/plexmediaserver
else
  echo "databasetools/plexmediaserver does not exist. Exiting."
  exit 1
fi

# Check the integrity of the database
db_path="/mnt/user/appdata/$dir/Plex Media Server/Plug-in Support/Databases/com.plexapp.plugins.library.db"
./Plex\ SQLite "$db_path" "PRAGMA integrity_check"
if [ $? -eq 0 ]; then
  echo "Database check passed. Exiting."
  exit 0
fi

# Database check failed, so create a backup
echo "Database check failed. Creating backup."
cp "$db_path" "$db_path"_old

# Recover the database
echo "Recovering database."
./Plex\ SQLite "$db_path" ".output recover.out" ".recover"

# Check the integrity of the recovered database
echo "Checking recovered database."
./Plex\ SQLite "$db_path" "PRAGMA integrity_check"
if [ $? -eq 0 ]; then
  echo "Database recovered successfully. Exiting."
  exit 0
fi

# Dump database
echo "Dumping database for reimport."
./"Plex SQLite" "$db_path" ".output dump.sql" ".dump"
rm "$db_path"

# Reread database
echo "Reimporting database."
./"Plex SQLite" "$db_path" ".read dump.sql"

#cleaning up
echo "Cleaning up."
leftover_path="/mnt/user/appdata/$dir/Plex Media Server/Plug-in Support/Databases/"
rm "$leftover_path"com.plexapp.plugins.library.db-shm
rm "$leftover_path"com.plexapp.plugins

#Make new database writeable
echo "Making new database writable"
chmod 777 "$db_path"

#Final check
echo "Running final database check"
./Plex\ SQLite "$db_path" "PRAGMA integrity_check"
exit 0